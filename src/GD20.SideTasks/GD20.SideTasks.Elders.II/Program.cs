﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GD20.SideTasks.Elders.II
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string input = Console.ReadLine();
            string result = CalculateOrderedSequences(input);
            Console.WriteLine(result);
        }

        public static string CalculateOrderedSequences(string input)
        {
            char[] chars = input
                .Replace("[", "")
                .Replace("]", "")
                .Where(c => c != ',')
                .OrderBy(x => char.IsUpper(x) ? 4 * x : x)
                .Distinct()
                .ToArray();

            StringBuilder resultBuilder = new StringBuilder();
            int i = 0;
            List<char> currentSeries = new List<char>();
            while (i < chars.Length)
            {
                char ch = chars[i];
                currentSeries.Clear();
                currentSeries.Add(ch);
                while (i + 1 < chars.Length && chars[i + 1] == ch + 1)
                {
                    currentSeries.Add(chars[i + 1]);
                    ch = chars[i + 1];
                    i++;
                }

                if (currentSeries.Count < 3)
                    resultBuilder.Append(string.Join(",", currentSeries));
                else
                {
                    resultBuilder.Append(currentSeries.First());
                    resultBuilder.Append('-');
                    resultBuilder.Append(currentSeries.Last());
                }

                resultBuilder.Append(',');
                i++;
            }

            resultBuilder.Remove(resultBuilder.Length - 1, 1); // remove last char (that is always ',')

            string output = resultBuilder.ToString();
            return output;
        }
    }
}