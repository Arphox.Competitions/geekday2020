﻿using FluentAssertions;
using GD20.SideTasks.Elders.III;
using NUnit.Framework;

namespace GD20.SideTasks.Elders.Tests
{
    [TestFixture]
    public class SideTask3Tests
    {
        [TestCase(8, 7, Program.Direction.Left, 7)]
        [TestCase(8, 5, Program.Direction.Left, 5)]
        [TestCase(56, 28, Program.Direction.Right, 36)]
        [TestCase(128, 42, Program.Direction.Left, 42)]
        [TestCase(98765432, 5263442, Program.Direction.Right, 40715738)]        // Orig result
        [TestCase(9876542365445, 5263442546547, Program.Direction.Left, 7424341233021)]
        public void TestCalculateSurvivor(long count, long start, Program.Direction direction, long expectedResult)
        {
            var survivor = Program.CalculateSurvivor(count, start, direction);
            survivor.Should().Be(expectedResult);
        }

        [TestCase(1, 2)]
        [TestCase(3, 4)]
        [TestCase(7, 8)]
        [TestCase(8, 8)]
        [TestCase(128, 128)]
        [TestCase(129, 256)]
        [TestCase(255, 256)]
        public void TestGetNearestBiggerPov(long count, long expectedPov)
        {
            var pov = Program.GetNearestBiggerPov(count);
            pov.Should().Be(expectedPov);
        }
    }
}