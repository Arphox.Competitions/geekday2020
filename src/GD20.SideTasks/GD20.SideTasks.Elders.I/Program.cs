﻿using System;
using System.Text;

namespace GD20.SideTasks.Elders.I
{
    public class Program
    {
        private static bool _isLogEnabled = true;
        private const char Separator = ' ';
        private const long MinRoomValue = -2L * 1000L * 1000L * 10000L;

        public static void Main(string[] args)
        {
            long[,] dungeon = ReadDungeon();
            Console.WriteLine(CalculateMinAmmo(dungeon));

            if (_isLogEnabled)
                Console.ReadLine();
        }

        private static long[,] ReadDungeon()
        {
            var dungeonSize = ReadDungeonSize();
            var dungeon = CreateEmptyDungeon(dungeonSize);
            FillDungeon(dungeon);

            return dungeon;
        }

        private static (int columnCount, int rowCount) ReadDungeonSize()
        {
            var sizeLine = Console.ReadLine().Split(Separator);

            int columnCount = int.Parse(sizeLine[0]);
            int rowCount = int.Parse(sizeLine[1]);

            return (columnCount, rowCount);
        }

        private static long[,] CreateEmptyDungeon((int columnCount, int rowCount) dungeonSize)
            => new long[dungeonSize.rowCount, dungeonSize.columnCount];

        private static void FillDungeon(long[,] dungeon)
        {
            var rowCount = dungeon.GetLength(0);
            for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
                FillRowIn(dungeon, rowIndex);
        }

        private static void FillRowIn(long[,] dungeon, int rowIndex)
        {
            string[] roomsInRow = Console.ReadLine().Split(Separator);
            for (var columnIndex = 0; columnIndex < roomsInRow.Length; columnIndex++)
                dungeon[rowIndex, columnIndex] = int.Parse(roomsInRow[columnIndex]);
        }

        public static string CalculateMinAmmo(long[,] dungeon)
        {
            var rowCount = dungeon.GetLength(0);
            var columnCount = dungeon.GetLength(1);

            long[,] adjacentDungeon = new long[rowCount, columnCount];
            long[,] resultDungeon = new long[rowCount, columnCount];

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
                {
                    long leftAdjacentValue = columnIndex == 0 ? MinRoomValue : adjacentDungeon[rowIndex, columnIndex - 1];
                    long upAdjacentValue = rowIndex == 0 ? MinRoomValue : adjacentDungeon[rowIndex - 1, columnIndex];
                    long roomValue = dungeon[rowIndex, columnIndex];

                    long roomAmmo = columnIndex == 0 && rowIndex == 0
                        ? roomValue
                        : Math.Max(leftAdjacentValue + roomValue, upAdjacentValue + roomValue);

                    adjacentDungeon[rowIndex, columnIndex] = roomAmmo;

                    long leftResultValue = columnIndex == 0 ? MinRoomValue : resultDungeon[rowIndex, columnIndex - 1];
                    long upResultValue = rowIndex == 0 ? MinRoomValue : resultDungeon[rowIndex - 1, columnIndex];

                    long resultAmmo = roomAmmo;

                    long maxResultValue = Math.Max(leftResultValue, upResultValue);
                    resultDungeon[rowIndex, columnIndex] = columnIndex == 0 && rowIndex == 0
                        ? roomValue
                        : Math.Min(maxResultValue, roomAmmo);
                }
            }

            string calculatorDungeons = string.Concat(
                LogDungeon(dungeon, nameof(dungeon)),
                LogDungeon(adjacentDungeon, nameof(adjacentDungeon)),
                LogDungeon(resultDungeon, nameof(resultDungeon)));

            long result = resultDungeon[rowCount - 1, columnCount - 1] * -1 + 1;

            return result.ToString();
        }

        private static string LogDungeon(long[,] helperDungeon, string title)
        {
            var builder = new StringBuilder();
            builder.AppendLine(title);

            var rowCount = helperDungeon.GetLength(0);
            var columnCount = helperDungeon.GetLength(1);

            for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    builder.Append($"{helperDungeon[rowIndex, columnIndex], 4}");
                }

                builder.AppendLine();
            }

            string message = builder.ToString();
            Log(message);
            return message;
        }

        private static void Log(string message)
        {
            if (!_isLogEnabled)
                return;

            Console.WriteLine($"--Log: {message}");
        }
    }
}
