﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// ReSharper disable FunctionNeverReturns
namespace GD20.UdpClientServerSamePortExample
{
    // Start this on another computer as well with the correct remote ip

    internal class Program
    {
        private const string REMOTE_IP = "192.168.1.169";
        private const int PORT = 55555;

        private static void Main()
        {
            using UdpClient client = new UdpClient(PORT, AddressFamily.InterNetwork);
            client.Connect(REMOTE_IP, PORT);

            byte[] initmsg = "1".AsUtf8Bytes();
            client.Send(initmsg, initmsg.Length);

            while (true)
            {
                // Wait for message
                IPEndPoint remoteEndpoint = null;
                byte[] receivedBytes = client.Receive(ref remoteEndpoint);

                // Print received message
                string receivedString = receivedBytes.AsUtf8String();
                Console.WriteLine(receivedString);

                // Parse as integer, increase by 1
                int num = int.Parse(receivedString);
                num++;

                // Send the increased number
                byte[] message = num.ToString().AsUtf8Bytes();
                client.Send(message, message.Length);

                Thread.Sleep(200);
            }
        }
    }

    internal static class Extensions
    {
        internal static string AsUtf8String(this byte[] bytes)
            => Encoding.UTF8.GetString(bytes);

        internal static byte[] AsUtf8Bytes(this string str)
            => Encoding.UTF8.GetBytes(str);
    }
}
