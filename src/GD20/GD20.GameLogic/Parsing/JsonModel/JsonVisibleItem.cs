﻿using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing.JsonModel
{
    public class JsonVisibleItem
    {
        [JsonProperty("X")]
        public int X { get; set; }

        [JsonProperty("Y")]
        public int Y { get; set; }

        [JsonProperty("ItemId")]
        public int ItemId { get; set; }

        public override string ToString() => $"{ItemId}@({X},{Y})";
    }
}
