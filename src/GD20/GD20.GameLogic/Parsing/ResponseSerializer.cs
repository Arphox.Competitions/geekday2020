﻿using System.Collections.Generic;
using System.Linq;
using GD20.GameLogic.GameModel;
using GD20.GameLogic.Parsing.JsonModel;
using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing
{
    public static class ResponseSerializer
    {
        private static readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore
        };

        public static string Serialize(IEnumerable<Command> commands)
        {
            var jsonCommands = commands.Select(x => new JsonResponseItem(x));
            return JsonConvert.SerializeObject(jsonCommands, serializerSettings);
        }
    }
}
