﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using GD20.Common;
using GD20.Common.Settings;
using GD20.GameLogic.GameModel;
using Serilog;

namespace GD20.GameLogic
{
    public class TargetFinder
    {
        private readonly ILogger _logger;

        public TargetFinder(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void FindTargetsForOwnUnits(PermanentMap map, MapInfo mapInfo)
        {
            _logger.Debug("FindTargetsForOwnUnits start...");

            List<GameItem> targetedEnemies = new List<GameItem>();

            foreach (OwnUnit ownUnit in mapInfo.OwnUnits)
            {
                ownUnit.Targets = GetTargets(map, mapInfo, ownUnit);
                targetedEnemies.AddRange(ownUnit.Targets);
            }

            int targetedDistinctEnemyCount = targetedEnemies.DistinctBy(x => x.ItemId).Count();
            int enemyCount = mapInfo.EnemyUnits.Count;
            _logger.Information("There are {EnemyCount} enemies, {TargetCount} targets, means we are {Diff} behind.",
                enemyCount, targetedDistinctEnemyCount, enemyCount - targetedDistinctEnemyCount);
        }

        /// <summary>
        /// Using the given <paramref name="map"/> and the given <paramref name="mapInfo"/>,
        /// finds all enemies those are clear shots for the given <paramref name="ownUnit"/>.
        /// </summary>
        private static IReadOnlyList<GameItem> GetTargets(PermanentMap map, MapInfo mapInfo, OwnUnit ownUnit)
        {
            return mapInfo.EnemyUnits
                .Where(enemy => CanShoot(map.Matrix, ownUnit.ToVector(), enemy.ToVector()))
                .ToArray();
        }

        public static bool CanShoot(int[,] matrix, Vector2 unit, Vector2 target, int sightDistance = GameSettings.SightDistance)
        {
            float originalDistance = Vector2.Distance(unit, target);
            if (originalDistance > sightDistance)
                return false; // too far

            Vector2 currentPos = unit;
            Vector2 normalized = MathHelper.CalculateNormalizedDirectionVector(unit, target);

            originalDistance = (float)Math.Ceiling(originalDistance);
            for (int i = 0; i < originalDistance; i++)
            {
                currentPos += normalized;
                int x = (int)Math.Round(currentPos.X, 0);
                int y = (int)Math.Round(currentPos.Y, 0);

                if (x < 0 || x >= matrix.GetLength(0) ||
                    y < 0 || y >= matrix.GetLength(1))
                    return false; // out of map

                if (matrix[x, y] == (int)ItemType.Wall)
                    return false; // wall blocks the way
            }

            return true; // we got to the target
        }
    }
}
