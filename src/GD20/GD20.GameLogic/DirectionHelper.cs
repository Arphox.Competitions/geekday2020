﻿using System;
using GD20.GameLogic.GameModel;

namespace GD20.GameLogic
{
    public static class DirectionHelper
    {
        private static readonly Random random = new Random();

        public static CommandType GetRandomDirection()
        {
            int rnd = random.Next(0, 8);
            return rnd switch
            {
                0 => CommandType.MoveNorth,
                1 => CommandType.MoveSouth,
                2 => CommandType.MoveEast,
                3 => CommandType.MoveWest,

                4 => CommandType.MoveNorthEast,
                5 => CommandType.MoveNorthWest,
                6 => CommandType.MoveSouthEast,
                7 => CommandType.MoveSouthWest,

                _ => throw new Exception("wtf")
            };
        }
    }
}
