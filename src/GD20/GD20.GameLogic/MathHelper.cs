﻿using System.Numerics;

namespace GD20.GameLogic
{
    public static class MathHelper
    {
        /// <summary>
        /// Calculate the direction vector from p1 towards p2.
        /// </summary>
        public static Vector2 CalculateDirectionVector(Vector2 p1, Vector2 p2)
            => new Vector2(p2.X - p1.X, p2.Y - p1.Y);

        /// <summary>
        /// Calculate the normalized direction vector from p1 towards p2.
        /// </summary>
        public static Vector2 CalculateNormalizedDirectionVector(Vector2 p1, Vector2 p2)
            => Vector2.Normalize(CalculateDirectionVector(p1, p2));
    }
}
