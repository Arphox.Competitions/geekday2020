﻿namespace GD20.GameLogic.GameModel
{
    public sealed class CommandType
    {
        public static readonly CommandType MoveNorth = new CommandType("MoveN", 2, 0);
        public static readonly CommandType MoveSouth = new CommandType("MoveS", 2, 0);
        public static readonly CommandType MoveEast = new CommandType("MoveE", 2, 0);
        public static readonly CommandType MoveWest = new CommandType("MoveW", 2, 0);
        public static readonly CommandType MoveNorthWest = new CommandType("MoveNW", 3, 0);
        public static readonly CommandType MoveNorthEast = new CommandType("MoveNE", 3, 0);
        public static readonly CommandType MoveSouthWest = new CommandType("MoveSW", 3, 0);
        public static readonly CommandType MoveSouthEast = new CommandType("MoveSE", 3, 0);
        public static readonly CommandType LieDown = new CommandType("LieDown", 8, 9);
        public static readonly CommandType Crouch = new CommandType("Crouch", 4, 6);
        public static readonly CommandType StandUp = new CommandType("StandUp", 4, 4);
        public static readonly CommandType Shoot = new CommandType("Shoot", 2, 0);

        public string RawCommand { get; }
        public int Cost { get; }

        /// <summary>
        /// The damage done in the state (NOT the damage done by the command)
        /// </summary>
        public int DamageDone { get; }

        private CommandType(string rawCommand, int cost, int damageDone)
        {
            RawCommand = rawCommand;
            Cost = cost;
            DamageDone = damageDone;
        }
    }
}
