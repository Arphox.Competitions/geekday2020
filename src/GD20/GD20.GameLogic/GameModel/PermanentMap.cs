﻿using System;
using System.Collections.Generic;
using GD20.Common.Settings;
using Serilog;

namespace GD20.GameLogic.GameModel
{
    public sealed class PermanentMap
    {
        private readonly ILogger _logger;

        public PermanentMap(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// [X,Y]
        /// </summary>
        public int[,] Matrix { get; } = new int[GameSettings.MapSizeX, GameSettings.MapSizeY];

        private readonly List<GameItem> _temporaryChanges = new List<GameItem>(2000);

        public void Accept(MapInfo mapInfo)
        {
            _logger.Debug("PermanentMap.Accept start...");

            foreach (GameItem gameItem in mapInfo.AllItems)
            {
                Matrix[gameItem.X, gameItem.Y] = (int)gameItem.ItemType;
                if (gameItem.ItemType != ItemType.Wall)
                    _temporaryChanges.Add(gameItem);
            }
        }

        public void RevertTemporaryChanges()
        {
            foreach (GameItem gameItem in _temporaryChanges)
                Matrix[gameItem.X, gameItem.Y] = 0;

            _temporaryChanges.Clear();
        }
    }
}
