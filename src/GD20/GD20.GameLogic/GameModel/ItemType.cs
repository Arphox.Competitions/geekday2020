﻿namespace GD20.GameLogic.GameModel
{
    public enum ItemType
    {
        Wall = 1,
        BonusAmmo = 2,
        BonusHealth = 3,
        OwnUnit = 4,
        EnemyUnit = 5,
    }
}
