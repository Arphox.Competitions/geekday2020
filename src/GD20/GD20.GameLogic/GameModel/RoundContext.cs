﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GD20.Common.Settings;
using Serilog;

namespace GD20.GameLogic.GameModel
{
    public sealed class RoundContext
    {
        private readonly ILogger _logger;
        private readonly List<Command> _commands = new List<Command>();

        public MapInfo MapInfo { get; }
        public int RemainingMovementPoints { get; private set; } = GameSettings.InitialMovementPoints;

        public IReadOnlyCollection<Command> Commands => _commands;

        public RoundContext(MapInfo mapInfo, ILogger logger)
        {
            MapInfo = mapInfo ?? throw new ArgumentNullException(nameof(mapInfo));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public bool OutOfMovementPoints => RemainingMovementPoints <= 0;

        /// <summary>
        /// Tries to add the command. Returns true if success, returns false if no more movements points for it.
        /// </summary>
        public bool TryAddCommand(Command command)
        {
            int cost = command.CommandType.Cost;
            if (RemainingMovementPoints < cost)
            {
                _logger.Debug("Out of movement points.");
                return false;
            }

            _commands.Add(command);
            RemainingMovementPoints -= cost;

            if (command.CommandType == CommandType.Shoot)
                command.Unit.Ammo--;

            return true;
        }

        public void RoundRobinizeAllCommands()
        {
            if (_commands.Count == 0) return;

            var commandsByUnits = _commands
                .GroupBy(x => x.Unit)
                .Select(x => new
                {
                    Unit = x.Key,
                    Commands = x.ToArray()
                })
                .ToArray();

            _commands.Clear();
            int maxSize = commandsByUnits.Max(x => x.Commands.Length);

            // Take the i. command
            for (int i = 0; i < maxSize; i++)
            {
                // For all units
                foreach (var commandByUnit in commandsByUnits)
                {
                    if (commandByUnit.Commands.Length <= i)
                        break;

                    _commands.Add(commandByUnit.Commands[i]);
                }
            }
        }

        public void LogCommandsToBeSent()
        {
            StringBuilder sb = new StringBuilder();
            int commandMovementPointSum = _commands.Sum(x => x.CommandType.Cost);
            sb.AppendLine($"Commands to be sent (used up {commandMovementPointSum}/{GameSettings.InitialMovementPoints}):");
            if (_commands.Count > 0)
                sb.Append(string.Join(Environment.NewLine, _commands.Select(x => x.ToString())));
            else
                sb.Append("<NONE>");

            _logger.Information(sb.ToString());
        }

        public void TryFilterOverkills()
        {
            // enemy ItemId -> shots fired
            Dictionary<int, int> enemyIdToShotsFired = MapInfo.EnemyUnits.ToDictionary(key => key.ItemId, value => 0);

            Command[] shootCommands = _commands.Where(c => c.CommandType == CommandType.Shoot).ToArray();
            foreach (Command command in shootCommands)
            {
                if (command.TargetId == null) continue; // Oversafety check
                int currentValue = enemyIdToShotsFired[(int)command.TargetId];
                if (currentValue >= 3)
                {
                    _logger.Information("Removing command '{Cmd}' because it would be probably overkill on target.", command);
                    _commands.Remove(command); // safe to remove as we are not iterating the original collection
                }

                enemyIdToShotsFired[(int)command.TargetId]++;
            }
        }
    }
}
