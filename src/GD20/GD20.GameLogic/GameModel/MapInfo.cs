﻿using System;
using System.Collections.Generic;

namespace GD20.GameLogic.GameModel
{
    public sealed class MapInfo
    {
        public IReadOnlyCollection<GameItem> AllItems { get; }
        public IReadOnlyList<OwnUnit> OwnUnits { get; }
        public IReadOnlyCollection<GameItem> EnemyUnits { get; }
        public IReadOnlyCollection<GameItem> Bonuses { get; }

        public MapInfo(
            IReadOnlyCollection<GameItem> allItems,
            IReadOnlyList<OwnUnit> myUnits,
            IReadOnlyCollection<GameItem> enemyUnits,
            IReadOnlyCollection<GameItem> bonuses)
        {
            OwnUnits = myUnits ?? throw new ArgumentNullException(nameof(myUnits));
            EnemyUnits = enemyUnits ?? throw new ArgumentNullException(nameof(enemyUnits));
            Bonuses = bonuses ?? throw new ArgumentNullException(nameof(bonuses));
            AllItems = allItems ?? throw new ArgumentNullException(nameof(allItems));
        }
    }
}
