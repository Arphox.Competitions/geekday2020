﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GD20.GameLogic.Data;
using GD20.GameLogic.GameModel;
using GD20.GameLogic.Parsing;
using GD20.GameLogic.Parsing.JsonModel;
using GD20.Networking.HttpLight.Model;
using Newtonsoft.Json;
using Serilog;

namespace GD20.GameLogic
{
    public sealed class Commander
    {
        private readonly ILogger _logger;
        private readonly PermanentMap _permanentMap;
        private readonly TargetFinder _targetFinder;

        public Commander(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _targetFinder = new TargetFinder(logger);
            _permanentMap = new PermanentMap(logger);
            WarmupNewtonsoftJson();
        }

        public LightHttpResponse Answer(LightHttpRequest httpRequest)
        {
            if (httpRequest.HttpMethod != "POST")
                return TextResponse.CreateUtf8Success("Sorry I only accept POST requests.");

            MapInfo mapInfo = RequestParser.Parse(httpRequest.BodyBytes, _logger);
            _permanentMap.Accept(mapInfo);
            _targetFinder.FindTargetsForOwnUnits(_permanentMap, mapInfo);
            LogOwnUnitData(mapInfo);
            RoundContext roundContext = new RoundContext(mapInfo, _logger.ForContext<RoundContext>());

            AddShootCommands(roundContext);
            while (AddMovingCommands(roundContext)) { }

            _permanentMap.RevertTemporaryChanges(); // this HAS to be the last step!
            roundContext.LogCommandsToBeSent();
            return TextResponse.CreateUtf8Success(ResponseSerializer.Serialize(roundContext.Commands));
        }

        private void AddShootCommands(RoundContext roundContext)
        {
            if (roundContext.OutOfMovementPoints) return;
            foreach (OwnUnit unit in roundContext.MapInfo.OwnUnits)
            {
                if (unit.HasNoTarget || unit.OutOfAmmo) continue; // exclude this unit from shooting logic

                bool lieDownShooting = CanDoLieDownShootingTactic(roundContext, unit);
                if (lieDownShooting) roundContext.TryAddCommand(new Command(unit, CommandType.LieDown));

                foreach (GameItem target in unit.Targets)
                {
                    Command command = new Command(unit, CommandType.Shoot, target.ItemId);

                    // If single target, and we have a lot of ammo, shoot twice
                    if (unit.Targets.Count == 1 && unit.Ammo >= 4)
                        if (!roundContext.TryAddCommand(command)) return; // Out of movement points

                    if (!roundContext.TryAddCommand(command)) return; // Out of movement points
                    if (unit.OutOfAmmo) break;
                }

                if (lieDownShooting) roundContext.TryAddCommand(new Command(unit, CommandType.StandUp));
            }

            roundContext.RoundRobinizeAllCommands();
            roundContext.TryFilterOverkills();
        }

        private bool CanDoLieDownShootingTactic(RoundContext roundContext, OwnUnit unit)
        {
            bool b = roundContext.RemainingMovementPoints >= // Do I have enough movement points for
                     CommandType.LieDown.Cost +
                     CommandType.StandUp.Cost +
                     CommandType.Shoot.Cost * unit.Targets.Count; // 1 shoot for all target?

            if (b)
                _logger.Information("Unit {UnitId} doing lieDown shooting tactic for {TargetCount} targets.", unit.ItemId, unit.Targets.Count);

            return b;
        }

        /// <summary>
        /// Returns whether it dispatched any commands. True -> dispatched command, false -> did not
        /// </summary>
        private bool AddMovingCommands(RoundContext roundContext)
        {
            if (roundContext.OutOfMovementPoints) return false;

            // Movable unit = No target or no ammo
            OwnUnit[] movableUnits = roundContext.MapInfo.OwnUnits
                .Where(x => x.HasNoTarget || x.OutOfAmmo)
                .ToArray();

            // With all movable units, move once to a random direction
            bool addedAnyCommand = false;
            foreach (OwnUnit unit in movableUnits)
            {
                bool canMove = MoveOnceToRandomDirection(roundContext, unit);
                if (canMove)
                    addedAnyCommand = true;
                else
                    return addedAnyCommand; // out of Movement points
            }

            return addedAnyCommand;
        }

        private static bool MoveOnceToRandomDirection(RoundContext roundContext, OwnUnit ownUnit)
        {
            Command cmd = new Command(ownUnit, DirectionHelper.GetRandomDirection());
            return roundContext.TryAddCommand(cmd);
        }

        private void LogOwnUnitData(MapInfo mapInfo)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("Our unit data:");
            str.Append(string.Join(Environment.NewLine, mapInfo.OwnUnits.Select(x => x.ToStringDetailed())));
            _logger.Information(str.ToString());
        }

        private void WarmupNewtonsoftJson()
        {
            JsonConvert.DeserializeObject<JsonRequest>(StaticData.TestJsonData);
        }
    }
}
