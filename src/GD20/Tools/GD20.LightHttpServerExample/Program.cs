﻿using System;
using GD20.Networking.HttpLight;
using GD20.Networking.HttpLight.ContextHandlers;
using GD20.Networking.HttpLight.Model;
using Serilog;

namespace GD20.LightHttpServerExample
{
    internal class Program
    {
        private static void Main()
        {
            //ILogger fileLogger = new LoggerConfiguration()
            //    .MinimumLevel.Verbose()
            //    .WriteTo.File(
            //        path: @"C:\temp\log.txt",
            //        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
            //    .CreateLogger();

            ILogger consoleLogger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            Func<LightHttpRequest, LightHttpResponse> responderMethod = _ =>
            {
                string message = $"Hello world! at {DateTime.Now.ToString("o")}";
                return TextResponse.CreateUtf8Success(message);
            };

            using var httpServerLight = new LightweightHttpServerBuilder()
                .DisableConcurrentRequestServing()
                .WithLogger(consoleLogger.ForContext<LightweightHttpServer>())
                .Add<ResponseTimeMeasurerContextHandler>(consoleLogger) // only gets logged to the console
                .Add<ExceptionHandlingContextHandler>()
                //.Add<IpBlacklistContextHandler>(new object[] { new string[] { "192.168.1.199" } })
                //.Add<IpWhitelistContextHandler>(new object[] { new string[] { "192.168.1.169" } })
                .Add<BusinessLogicContextHandler>(responderMethod)
                .AddPrefixToListen("http://*:54001/") // if you don't add any, it will be the default :80
                .Build();

            httpServerLight.Start();

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
