﻿using GD20.Networking.HttpLight.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace GD20.Networking.HttpLight
{
    public sealed class LightweightHttpServer : IDisposable
    {
        public const string DefaultPrefix = "http://*/";

        private readonly HttpListener _listener;
        private readonly bool _enableConcurrentRequestServing;
        private readonly IContextHandler _contextHandler;
        private readonly string _contextHandlerType;
        private ILogger _logger;

        public bool IsListening => _listener.IsListening;
        public bool IsDisposed { get; private set; } = false;

        public LightweightHttpServer(
            IContextHandler contextHandler,
            ILogger logger,
            bool enableConcurrentRequestServing = true)
            : this(contextHandler, logger, new[] { DefaultPrefix }, enableConcurrentRequestServing)
        {
        }

        public LightweightHttpServer(
            IContextHandler contextHandler,
            ILogger logger,
            IReadOnlyCollection<string> prefixes,
            bool enableConcurrentRequestServing = true)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _contextHandler = contextHandler ?? throw new ArgumentNullException(nameof(contextHandler));
            _contextHandlerType = _contextHandler.GetType().FullName;
            _enableConcurrentRequestServing = enableConcurrentRequestServing;

            if (prefixes is null) throw new ArgumentNullException(nameof(prefixes));
            if (prefixes.Count == 0) throw new ArgumentException("No prefix provided");

            _listener = new HttpListener();

            foreach (string prefix in prefixes)
                _listener.Prefixes.Add(prefix);

            _logger.Information($"Initializing {nameof(LightweightHttpServer)} with prefixes: {{@prefixes}}", prefixes);
        }

        public void ChangeLogger(ILogger newLogger) => _logger = newLogger;

        public void Start()
        {
            ThrowIfDisposed();
            _logger.Information("Starting server");
            _listener.Start();

            ThreadPool.QueueUserWorkItem(o =>
            {
                if (IsDisposed) return;
                try
                {
                    RunWorkItem();
                }
                catch (ObjectDisposedException e)
                {
                    _logger.Error(e, "ObjectDisposedException thrown");
                }
            });
        }

        private void RunWorkItem()
        {
            while (!IsDisposed && _listener.IsListening)
            {
                HttpListenerContext context = _listener.GetContext(); // blocking wait until the next request

                if (_enableConcurrentRequestServing)
                {
                    _logger.Verbose("Request incoming, queueing...");
                    ThreadPool.QueueUserWorkItem(_ =>
                    {
                        if (IsDisposed) return;
                        _logger.Debug($"Handing context to '{_contextHandlerType}'");
                        _contextHandler.Handle(context);
                    });
                }
                else
                {
                    _logger.Debug($"Handing context to '{_contextHandlerType}'");
                    _contextHandler.Handle(context);
                }
            }
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            _logger.Debug("Dispose requested");

            IsDisposed = true;
            try
            {
                _listener.Stop();
                _listener.Close();
            }
            catch (Exception e)
            {
                _logger.Warning(e, "Exception occurred while stopping the HttpListener.");
            }
        }

        private void ThrowIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(GetType().FullName);
        }
    }
}
