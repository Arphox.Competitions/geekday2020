﻿using GD20.Networking.HttpLight.Model;
using Serilog;
using System;
using System.Text;

namespace GD20.Networking.HttpLight.Helpers
{
    internal static class RequestResponseLogger
    {
        private static readonly string NL = Environment.NewLine;

        public static void LogRequest(ILogger logger, LightHttpRequest request)
        {
            string bodyPart = GetOptionalBodyPart(request.ContentEncoding, request.BodyBytes);

            logger.Debug($"{NL}[REQUEST] {{reqmethod}} {{requrl}} FROM {{reqip}}{{optionalbody}}",
                request.HttpMethod,
                request.Url,
                request.RemoteEndPoint.Address.ToString(),
                bodyPart);
        }

        public static void LogResponse(ILogger logger, LightHttpResponse response)
        {
            string bodyPart = GetOptionalBodyPart(response.ContentEncoding, response.BodyBytes);

            logger.Debug($"{NL}[RESPONSE] {{code}} ({{codedesc}}){{optionalbody}}",
                response.StatusCode,
                response.StatusDescription,
                bodyPart);
        }

        private static string GetOptionalBodyPart(Encoding encoding, byte[] bodyBytes)
        {
            string bodyString = encoding.GetString(bodyBytes);
            if (string.IsNullOrEmpty(bodyString))
                return string.Empty;
            else
                return $"{NL}Body: \"{bodyString}\"";
        }
    }
}
