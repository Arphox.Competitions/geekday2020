﻿using System.Net;
using System.Text;

namespace GD20.Networking.HttpLight.Model
{
    public class LightHttpResponse
    {
        public int StatusCode { get; }
        public string StatusDescription { get; }
        public byte[] BodyBytes { get; }
        public Encoding ContentEncoding { get; }
        public string ContentType { get; }
        public WebHeaderCollection Headers { get; }

        public LightHttpResponse(
            int statusCode,
            string statusDescription,
            byte[] bodyBytes,
            Encoding contentEncoding,
            string contentType,
            WebHeaderCollection headers = null)
        {
            StatusCode = statusCode;
            StatusDescription = statusDescription;
            BodyBytes = bodyBytes;
            ContentEncoding = contentEncoding;
            ContentType = contentType;
            Headers = headers;
        }
    }
}