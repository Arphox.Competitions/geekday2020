﻿using System.Net;

namespace GD20.Networking.HttpLight.Interfaces
{
    public interface IContextHandler
    {
        void Handle(HttpListenerContext context);
    }
}