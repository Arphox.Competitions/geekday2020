﻿using GD20.Networking.HttpLight.ContextHandlers;
using GD20.Networking.HttpLight.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GD20.Networking.HttpLight
{
    public sealed class LightweightHttpServerBuilder
    {
        /// <summary>
        ///     Contains all desired handlers; the last handler is on the top of the stack.
        ///     Type -> type of the IContextHandler implementation
        ///     object[] -> constructor parameters for the type to create
        ///     If the first parameter is exactly this reference: <see cref="_nextContextHandlerPointer"/>,
        ///         it means you have to set that object to the next handler in the chain.
        /// </summary>
        private readonly Stack<(Type, object[])> _handlers = new Stack<(Type, object[])>();
        private readonly object _nextContextHandlerPointer = new object(); // just a special reference
        private ILogger _logger;
        private bool _enableConcurrentRequestServing = true;
        private readonly List<string> _listeningPrefixes = new List<string>();

        public LightweightHttpServerBuilder WithLogger(ILogger logger)
        {
            if (logger is null) throw new ArgumentNullException(nameof(logger));
            if (_logger != null) throw new InvalidOperationException("The logger is already set.");

            _logger = logger;
            return this;
        }
        public LightweightHttpServerBuilder EnableConcurrentRequestServing()
        {
            _enableConcurrentRequestServing = true;
            return this;
        }
        public LightweightHttpServerBuilder DisableConcurrentRequestServing()
        {
            _enableConcurrentRequestServing = false;
            return this;
        }

        public LightweightHttpServerBuilder AddPrefixToListen(string prefix)
        {
            _listeningPrefixes.Add(prefix);
            return this;
        }

        public LightweightHttpServerBuilder Add<THandler>(params object[] additionalParameters)
            where THandler : IContextHandler
        {
            return Add<THandler>(_logger, additionalParameters);
        }

        public LightweightHttpServerBuilder Add<THandler>(ILogger logger, params object[] additionalParameters)
            where THandler : IContextHandler
        {
            ConstructorInfo ctor = GetSinglePublicConstructor<THandler>();
            List<ParameterInfo> parameters = ctor.GetParameters().ToList();

            List<object> ctorParams = new List<object>();
            AddNextContextHandlerPointerIfNeeded(parameters, ctorParams);

            int additionalParamIndexTracker = 0;
            foreach (ParameterInfo pInfo in parameters)
            {
                if (pInfo.ParameterType == typeof(IContextHandler))
                {
                    throw new ArgumentException($"The public constructor of type '{typeof(THandler).FullName}'" +
                        $"can only have a {nameof(IContextHandler)} parameter as the first argument.");
                }
                else if (pInfo.ParameterType == typeof(ILogger))
                {
                    ctorParams.Add(logger.ForContext<THandler>());
                }
                else
                {
                    if (additionalParameters.Length < additionalParamIndexTracker + 1)
                        throw new ArgumentException("Not enough parameters supplied for the given type!");

                    Type requiredType = pInfo.ParameterType;
                    object nextParam = additionalParameters[additionalParamIndexTracker];
                    Type actualType = nextParam.GetType();
                    if (requiredType.IsAssignableFrom(actualType))
                    {
                        ctorParams.Add(nextParam);
                    }
                    else
                    {
                        throw new ArgumentException($"Incorrectly typed argument supplied at index {additionalParamIndexTracker}." +
                            $" Required {requiredType.FullName} but was given {actualType.FullName}");
                    }

                    additionalParamIndexTracker++;
                }
            }

            _handlers.Push((typeof(THandler), ctorParams.ToArray()));
            return this;
        }

        private bool AddNextContextHandlerPointerIfNeeded(List<ParameterInfo> parameterInfos, List<object> ctorParams)
        {
            if (parameterInfos[0].ParameterType == typeof(IContextHandler))
            {
                ctorParams.Add(_nextContextHandlerPointer);
                parameterInfos.RemoveAt(0);
                return true;
            }

            return false;
        }

        public LightweightHttpServer Build()
        {
            _logger ??= new LoggerConfiguration().CreateLogger();
            IContextHandler entryHandler = CreateContextHandlerChain();

            var prefixes = _listeningPrefixes;
            if (prefixes.Count == 0)
                prefixes.Add(LightweightHttpServer.DefaultPrefix);

            return new LightweightHttpServer(
                entryHandler,
                _logger.ForContext<LightweightHttpServer>(),
                prefixes,
                _enableConcurrentRequestServing);
        }

        private IContextHandler CreateContextHandlerChain()
        {
            IContextHandler next = new EmptyContextHandler();

            while (_handlers.Any())
            {
                (Type handlerType, object[] ctorParams) = _handlers.Pop();
                if (ctorParams.Length > 0 && ctorParams[0] == _nextContextHandlerPointer)
                    ctorParams[0] = next;

                next = (IContextHandler)Activator.CreateInstance(handlerType, ctorParams);
            }

            return next;
        }
        private static ConstructorInfo GetSinglePublicConstructor<T>() where T : IContextHandler
        {
            ConstructorInfo[] ctors = typeof(T).GetConstructors();
            if (ctors.Length != 1)
                throw new InvalidOperationException($"Exactly one public constructor expected from handler '{typeof(T).FullName}'.");

            return ctors[0];
        }
    }
}
