﻿using GD20.Networking.HttpLight.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace GD20.Networking.HttpLight.ContextHandlers
{
    public sealed class IpBlacklistContextHandler : IContextHandler
    {
        private readonly IContextHandler _next;
        private readonly HashSet<string> _disabledIpAddresses;
        private readonly ILogger _logger;

        public IpBlacklistContextHandler(
            IContextHandler next,
            IReadOnlyCollection<string> disabledIpAddresses,
            ILogger logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            if (disabledIpAddresses is null)
                throw new ArgumentNullException(nameof(disabledIpAddresses));

            _disabledIpAddresses = new HashSet<string>(disabledIpAddresses);
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Handle(HttpListenerContext context)
        {
            string ip = context.Request.RemoteEndPoint.Address.ToString();
            if (_disabledIpAddresses.Contains(ip))
            {
                _logger.Information($"Ip {ip} is blacklisted, not responding.", ip);
                context.Response.StatusCode = 403; // Forbidden

                byte[] responseBytes = Encoding.UTF8.GetBytes($"Hey '{ip}', you are blacklisted, so fuck off! :-)");
                context.Response.ContentLength64 = responseBytes.Length;
                context.Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
            }
            else
            {
                _logger.Verbose($"Ip {ip} not on blacklist, proceeding to next handler...", ip);
                _next.Handle(context);
            }
        }
    }
}
