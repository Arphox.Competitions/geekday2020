﻿using GD20.Networking.HttpLight.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace GD20.Networking.HttpLight.ContextHandlers
{
    public sealed class IpWhitelistContextHandler : IContextHandler
    {
        private readonly IContextHandler _next;
        private readonly HashSet<string> _enabledIpAddresses;
        private readonly ILogger _logger;

        public IpWhitelistContextHandler(
            IContextHandler next,
            IReadOnlyCollection<string> enabledIpAddresses,
            ILogger logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            if (enabledIpAddresses is null)
                throw new ArgumentNullException(nameof(enabledIpAddresses));

            _enabledIpAddresses = new HashSet<string>(enabledIpAddresses.Concat(IpHelper.GetAllLocalMachineAddresses()));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Handle(HttpListenerContext context)
        {
            string ip = context.Request.RemoteEndPoint.Address.ToString();
            if (_enabledIpAddresses.Contains(ip))
            {
                _logger.Verbose($"Ip {ip} whitelisted, proceeding to next handler...", ip);
                _next.Handle(context);
            }
            else
            {
                _logger.Information($"Ip {ip} NOT on whitelist, not responding.", ip);
                context.Response.StatusCode = 403; // Forbidden

                byte[] responseBytes = Encoding.UTF8.GetBytes($"Hey '{ip}'! Sorry, but you are not on the whitelist.");
                context.Response.ContentLength64 = responseBytes.Length;
                context.Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
            }
        }
    }
}