﻿using System;
using System.Diagnostics;
using System.Net;
using GD20.Networking.HttpLight.Interfaces;
using Serilog;

namespace GD20.Networking.HttpLight.ContextHandlers
{
    public sealed class ResponseTimeMeasurerContextHandler : IContextHandler
    {
        private readonly IContextHandler _next;
        private readonly ILogger _logger;

        public ResponseTimeMeasurerContextHandler(IContextHandler next, ILogger logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Handle(HttpListenerContext context)
        {
            long start = Stopwatch.GetTimestamp();
            _logger.Verbose("Response time stopwatch started...");

            _next.Handle(context);
            double elapsedMs = GetElapsedMilliseconds(start, Stopwatch.GetTimestamp());
            _logger.Information("Response process time was {ms} ms.", elapsedMs);
        }

        private static double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }
    }
}