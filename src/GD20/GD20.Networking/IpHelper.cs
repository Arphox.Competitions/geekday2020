﻿using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace GD20.Networking
{
    public static class IpHelper
    {
        public static string[] GetAllLocalMachineAddresses()
        {
            string[] loopbacks =
            {
                "::1",       // IPv6 loopback
                "127.0.0.1", // IPv4 loopback
            };

            var host = Dns.GetHostEntry(Dns.GetHostName());

            var locals = host.AddressList
                .Select(x => x.ToString())
                .ToArray();

            return locals.Concat(loopbacks).ToArray();
        }

        public static string[] GetAllLocalIpv4Addresses()
        {
            string[] loopback = { "127.0.0.1" };

            var host = Dns.GetHostEntry(Dns.GetHostName());

            var locals = host.AddressList
                .Where(x => x.AddressFamily == AddressFamily.InterNetwork) // IPv4
                .Select(x => x.ToString())
                .ToArray();

            return locals.Concat(loopback).ToArray();
        }
    }
}