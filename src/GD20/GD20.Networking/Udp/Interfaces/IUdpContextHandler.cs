﻿namespace GD20.Networking.Udp.Interfaces
{
    public interface IUdpContextHandler
    {
        void Handle(UdpDatagramContext context);
    }
}