﻿using System;
using System.IO;

namespace GD20.Common.Settings
{
    public static class LogSettings
    {
        private const string LogFilesFolder = @"c:/gd/log/";
        private static string secondsTimeStamp = DateTime.Now.ToSecondsTimestamp();

        public static string GeneralLogFilePath => Path.Combine(LogFilesFolder, $"{secondsTimeStamp}.txt");
        public static string InfoPlusLogFilePath => Path.Combine(LogFilesFolder, $"{secondsTimeStamp}_INFO+.txt");

        public static void OpenNewContext()
        {
            secondsTimeStamp = DateTime.Now.ToSecondsTimestamp();
        }
    }
}
