﻿using System;
using System.Threading;

namespace GD20.Common
{
    // Based on: https://codeblog.jonskeet.uk/2009/11/04/revisiting-randomness/
    public static class ThreadLocalRandom
    {
        private static readonly Random globalRandom = new Random();
        private static readonly ThreadLocal<Random> threadRandom = new ThreadLocal<Random>(NewRandom);
        private static Random Instance => threadRandom.Value;

        private static Random NewRandom()
        {
            lock (globalRandom)
                return new Random(globalRandom.Next());
        }

        public static int Next() => Instance.Next();
        public static int Next(int maxValue) => Instance.Next(maxValue);
        public static int Next(int minValue, int maxValue) => Instance.Next(minValue, maxValue);
        public static double NextDouble() => Instance.NextDouble();
        public static void NextBytes(byte[] buffer) => Instance.NextBytes(buffer);
    }
}